package com.lottery.game.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;

import com.lottery.game.container.ContainerInitializer;
import com.lottery.game.entity.Game;
import com.lottery.game.entity.Ticket;
import com.lottery.game.enumeration.Status;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = ContainerInitializer.class)
class TicketRepositoryTest {

    @Autowired
    private TicketRepository ticketRepository;

    private Ticket ticket;

    private Page<Ticket> tickets;

    private String ticketId;

    private Optional<Ticket> optTicket;

    @BeforeEach
    void setup() {
        ticketRepository.deleteAll();
    }

    @Test
    void shouldCreateTicketSuccessfully() {
        givenAValidTicket();
        whenCallSaveTicket();
        thenExpectedTicketIdNotEmpty();
    }

    @Test
    void shouldFindTicketsSuccessfully() {
        givenTickets();
        whenCallTickets();
        thenExpectedTicketWithPagination();
    }

    @Test
    void shouldFindTicketByIdSuccessfully() {
        givenAValidTicket();
        whenCallSaveTicket();
        thenExpectedTicketIdNotEmpty();
        whenCallFindTicketById();
        thenExpectedTicketNotEmpty();
    }

    @Test
    void shouldUpdateTicketStatusSuccessfully() {
        givenAValidTicket();
        givenAValidTicketWithStatusChecked();
        whenCallSaveTicket();
        thenExpectedTicketIdNotEmpty();
        whenCallFindTicketById();
        thenExpectedTicketWithStatusChecked();
    }

    // given methods
    private void givenAValidTicket() {
        ticket = Ticket.builder()
                .createdAt(LocalDateTime.now())
                .status(Status.PENDING)
                .build();
        Game game = new Game(ticket, 0, 1, 1);
        ticket.setGames(List.of(game));
    }

    private void givenAValidTicketWithStatusChecked() {
        ticket.setStatus(Status.CHECKED);
    }

    private void givenTickets() {
        createTickets();
    }

    // when methods
    private void whenCallSaveTicket() {
        ticket = ticketRepository.save(ticket);
        ticketId = ticket.getId();
    }

    private void whenCallFindTicketById() {
        optTicket = ticketRepository.findById(ticketId);
    }

    private void whenCallTickets() {
        tickets = ticketRepository.findAll(PageRequest.of(0, 3));
    }

    // then methods
    private void thenExpectedTicketIdNotEmpty() {
        assertNotNull(ticketId);
    }

    private void thenExpectedTicketNotEmpty() {
        assertNotNull(optTicket.get());
    }

    private void thenExpectedTicketWithPagination() {
        assertEquals(3, tickets.getSize());
        assertEquals(10, tickets.getTotalElements());
    }

    private void thenExpectedTicketWithStatusChecked() {
        assertNotNull(optTicket.get());
        assertEquals(Status.CHECKED, optTicket.get().getStatus());
    }

    // other methods
    private void createTickets() {
        IntStream.range(0, 10).forEach(index -> {
            Ticket ticketEntity = Ticket.builder()
                    .status(Status.PENDING)
                    .build();
            Game game = new Game(ticketEntity, 0, 1, 1);
            ticketEntity.setGames(List.of(game));
            ticketEntity.setCreatedAt(LocalDateTime.now().plusSeconds(index));
            ticketRepository.save(ticketEntity);
        });
    }

}
