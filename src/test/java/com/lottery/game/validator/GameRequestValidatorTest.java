package com.lottery.game.validator;

import static org.junit.Assert.assertThrows;

import org.junit.jupiter.api.Test;

import com.lottery.game.exception.InvalidNumberException;
import com.lottery.game.request.GameRequest;

class GameRequestValidatorTest {

    @Test
    void shouldThrowInvalidNumberException() {
        // given
        GameRequest gameRequest = new GameRequest(null, 0, 1);
        assertThrows(InvalidNumberException.class, () -> GameRequestValidator.validate(gameRequest));
    }

}
