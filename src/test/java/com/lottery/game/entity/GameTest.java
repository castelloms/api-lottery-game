package com.lottery.game.entity;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class GameTest {

    @ParameterizedTest
    @MethodSource("provideGames")
    void shouldValidateResult(Game input, int expected) {
        // given
        Game game = input;
        // when
        game.calculateResult();
        // then
        Assertions.assertEquals(expected, game.getResult());
    }

    private static Stream<Arguments> provideGames() {
        return Stream.of(
                Arguments.of(new Game(null, 3, 1, 2), 0),
                Arguments.of(new Game(null, 0, 1, 1), 10),
                Arguments.of(new Game(null, 1, 1, 1), 5),
                Arguments.of(new Game(null, 2, 1, 1), 1),
                Arguments.of(new Game(null, 0, 1, 0), 0));
    }
}
