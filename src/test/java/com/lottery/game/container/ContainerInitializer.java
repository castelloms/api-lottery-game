package com.lottery.game.container;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.MySQLContainer;

public class ContainerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final MySQLContainer<?> CONTAINER = new MySQLContainer<>("mysql:8.0");

    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {

        CONTAINER.start();

        TestPropertyValues.of("spring.datasource.url=" + CONTAINER.getJdbcUrl(),
                "spring.datasource.username=" + CONTAINER.getUsername(),
                "spring.datasource.password=" + CONTAINER.getPassword())
                .applyTo(configurableApplicationContext.getEnvironment());
    }

}
