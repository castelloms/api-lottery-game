package com.lottery.game.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lottery.game.entity.Game;
import com.lottery.game.entity.Ticket;
import com.lottery.game.enumeration.Status;
import com.lottery.game.exception.ErrorMessage;
import com.lottery.game.exception.InvalidNumberOfGamesException;
import com.lottery.game.exception.Message;
import com.lottery.game.exception.ResourceNotFoundException;
import com.lottery.game.exception.TicketCheckedException;
import com.lottery.game.request.GameRequest;
import com.lottery.game.request.TicketRequest;
import com.lottery.game.response.GameResponse;
import com.lottery.game.response.TicketResponse;
import com.lottery.game.service.TicketService;

@WebMvcTest(TicketController.class)
class TicketControllerTest {

    private static final String TICKETS_URL = "/tickets";
    private static final String TICKETS_BY_ID_URL = "/tickets/{ticketId}";
    private static final String GAMES_BY_TICKET_ID_URL = "/tickets/{ticketId}/games";
    private static final String TICKETS_STATUS_URL = "/tickets/{ticketId}/status";
    private static final String VALID_TICKET_ID = UUID.randomUUID().toString();

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private MockMvc mvc;

    private MockHttpServletResponse response;

    @MockBean
    private TicketService ticketService;

    private TicketRequest ticketRequest;

    private GameRequest gameRequest;

    @Test
    void shouldCreateTicketSucessfully() throws Exception {
        givenTicketServiceCreateTicketSuccessfully();
        givenAValidTicketRequest();
        whenCallControllerCreateTicket();
        thenExpectCreatedStatus();
        thenExpectLocationHeader();
    }

    @Test
    void shouldntCreateTicketWithGamesQuantityNegative() throws Exception {
        givenTicketServiceCreateTicketThrowInvalidNumberOfGamesException();
        givenATicketRequestWithGamesQuantityNegative();
        whenCallControllerCreateTicket();
        thenExpectBadRequestStatus();
        thenExpectResponseBodyWithInvalidNumberOfGames();
    }

    @Test
    void shouldFindTicketByIdSuccessfully() throws Exception {
        givenTicketServiceFindTicketByIdSuccessfully();
        whenCallControllerFindTicketById();
        thenExpectOkStatus();
        thenExpectResponseBodyWithTicket();
    }

    @Test
    void shouldFindTicketsSuccessfully() throws Exception {
        givenTicketServiceFindTicketsSuccessfully();
        whenCallControllerFindTickets();
        thenExpectOkStatus();
        thenExpectResponseBodyWithOneTicket();
    }

    @Test
    void shouldAddGameToTicketSuccessfuly() throws Exception {
        givenTicketServiceUpdateTicketSuccessfully();
        givenAValidGameRequest();
        whenCallControllerAddGameToTicket();
        thenExpectNoContentStatus();
    }

    @Test
    void shouldntAddGameToTicketWithInvalidNumber() throws Exception {
        givenTicketServiceUpdateTicketThrowTicketCheckedException();
        givenAnInvalidGameRequest();
        whenCallControllerAddGameToTicket();
        thenExpectBadRequestStatus();
        thenExpectResponseBodyWithInvalidNumbers();
    }

    @Test
    void shouldntAddGameToTicketWithStatusChecked() throws Exception {
        givenTicketServiceUpdateTicketThrowTicketCheckedException();
        givenAValidGameRequest();
        whenCallControllerAddGameToTicket();
        thenExpectUnprocessableEntityStatus();
        thenExpectResponseBodyWithTicketChecked();
    }

    @Test
    void shouldntAddGameToTicketWithTicketIdNotFound() throws Exception {
        givenTicketServiceUpdateTicketThrowResourceNotFoundException();
        givenAValidGameRequest();
        whenCallControllerAddGameToTicket();
        thenExpectNotFoundStatus();
    }

    @Test
    void shouldUpdateTicketStatusSucessfully() throws Exception {
        givenTicketServiceUpdateTicketStatusSuccessfully();
        whenCallControllerUpdateTicketStatus();
        thenExpectOkStatus();
        thenExpectResponseBodyWithTicket();
    }

    @Test
    void shouldntUpdateTicketStatusWithTicketIdNotFound() throws Exception {
        givenTicketServiceUpdateTicketStatusThrowResourceNotFoundEception();
        whenCallControllerUpdateTicketStatus();
        thenExpectNotFoundStatus();
    }

    // given methods
    private void givenTicketServiceCreateTicketSuccessfully() {
        doReturn(buildTicketResponse()).when(ticketService).createTicket(any(TicketRequest.class));
    }

    private void givenTicketServiceCreateTicketThrowInvalidNumberOfGamesException() {
        doThrow(new InvalidNumberOfGamesException()).when(ticketService).createTicket(any(TicketRequest.class));
    }

    private void givenTicketServiceFindTicketByIdSuccessfully() {
        doReturn(buildTicketResponse()).when(ticketService).findTicketById(any(String.class));
    }

    private void givenTicketServiceFindTicketsSuccessfully() {
        List<Game> games = new ArrayList<>();
        Ticket ticket = Ticket.builder()
                .id(VALID_TICKET_ID)
                .games(games)
                .build();
        games.add(new Game(ticket, 0, 2, 0));
        doReturn(new PageImpl<>(List.of(ticket))).when(ticketService).findTickets(any(PageRequest.class));
    }

    private void givenTicketServiceUpdateTicketSuccessfully() {
        doNothing().when(ticketService).addGameToTicket(any(String.class), any(GameRequest.class));
    }

    private void givenTicketServiceUpdateTicketThrowTicketCheckedException() {
        doThrow(new TicketCheckedException()).when(ticketService).addGameToTicket(any(String.class),
                any(GameRequest.class));
    }

    private void givenTicketServiceUpdateTicketThrowResourceNotFoundException() {
        doThrow(new ResourceNotFoundException()).when(ticketService).addGameToTicket(any(String.class),
                any(GameRequest.class));
    }

    private void givenTicketServiceUpdateTicketStatusSuccessfully() {
        doReturn(buildTicketResponse()).when(ticketService).updateTicketStatus(any(String.class));
    }

    private void givenTicketServiceUpdateTicketStatusThrowResourceNotFoundEception() {
        doThrow(new ResourceNotFoundException()).when(ticketService).updateTicketStatus(any(String.class));
    }

    private void givenAValidTicketRequest() {
        ticketRequest = new TicketRequest(5);
    }

    private void givenATicketRequestWithGamesQuantityNegative() {
        ticketRequest = new TicketRequest(-1);
    }

    private void givenAValidGameRequest() {
        gameRequest = new GameRequest(0, 0, 2);
    }

    private void givenAnInvalidGameRequest() {
        gameRequest = new GameRequest(0, 5, 2);
    }

    // when methods
    private void whenCallControllerCreateTicket() throws Exception {
        response = mvc.perform(post(TICKETS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(ticketRequest)))
                .andReturn()
                .getResponse();
    }

    private void whenCallControllerFindTicketById() throws Exception {
        response = mvc.perform(get(TICKETS_BY_ID_URL, VALID_TICKET_ID))
                .andReturn()
                .getResponse();
    }

    private void whenCallControllerFindTickets() throws Exception {
        response = mvc.perform(get(TICKETS_URL))
                .andReturn()
                .getResponse();
    }

    private void whenCallControllerAddGameToTicket() throws Exception {
        response = mvc.perform(post(GAMES_BY_TICKET_ID_URL, VALID_TICKET_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(gameRequest)))
                .andReturn()
                .getResponse();
    }

    private void whenCallControllerUpdateTicketStatus() throws Exception {
        response = mvc.perform(patch(TICKETS_STATUS_URL, VALID_TICKET_ID))
                .andReturn()
                .getResponse();
    }

    // then methods
    private void thenExpectOkStatus() {
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    private void thenExpectCreatedStatus() {
        assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
    }

    private void thenExpectLocationHeader() {
        assertThat(response.getHeader("location")).isEqualTo("http://localhost/tickets/" + VALID_TICKET_ID);
    }

    private void thenExpectNoContentStatus() {
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NO_CONTENT.value());
    }

    private void thenExpectBadRequestStatus() {
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    }

    private void thenExpectNotFoundStatus() {
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    private void thenExpectUnprocessableEntityStatus() {
        assertThat(response.getStatus()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());
    }

    private void thenExpectResponseBodyWithTicket() throws IOException {
        String json = response.getContentAsString();
        TicketResponse ticketResponse = toObject(json, TicketResponse.class);
        assertEquals(VALID_TICKET_ID, ticketResponse.getTicketId());
        assertEquals(Status.PENDING, ticketResponse.getStatus());
        assertEquals(1, ticketResponse.getGamesCount());
        assertEquals(10, ticketResponse.getGames().get(0).getResult());
    }

    private void thenExpectResponseBodyWithOneTicket() throws IOException {
        String json = response.getContentAsString();
        List<TicketResponse> tickets = toObjectList(json, TicketResponse.class);
        assertEquals(1, tickets.size());
    }

    private void thenExpectResponseBodyWithInvalidNumbers() throws IOException {
        String json = response.getContentAsString();
        Message message = toObject(json, Message.class);
        assertEquals(ErrorMessage.INVALID_NUMBER, message.getError());
    }

    private void thenExpectResponseBodyWithInvalidNumberOfGames() throws IOException {
        String json = response.getContentAsString();
        Message message = toObject(json, Message.class);
        assertEquals(ErrorMessage.INVALID_NUMBER_OF_GAMES, message.getError());
    }

    private void thenExpectResponseBodyWithTicketChecked() throws IOException {
        String json = response.getContentAsString();
        Message message = toObject(json, Message.class);
        assertEquals(ErrorMessage.TICKET_CHECKED, message.getError());
    }

    // other methods
    private TicketResponse buildTicketResponse() {
        return TicketResponse.builder()
                .ticketId(VALID_TICKET_ID)
                .creationDate(LocalDateTime.now())
                .status(Status.PENDING)
                .gamesCount(1)
                .games(List.of(new GameResponse(List.of(0, 1, 1), 10)))
                .build();
    }

    private String toJson(Object object) {
        try {
            return mapper.writeValueAsString(object);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    private <T> T toObject(String json, Class<T> clazz) {
        try {
            return mapper.readValue(json, clazz);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    protected <T> List<T> toObjectList(String json, Class<T> clazz) {
        try {
            return mapper.readValue(json, new TypeReference<List<T>>() {
                @Override
                public Type getType() {
                    return mapper.getTypeFactory().constructCollectionType(List.class, clazz);
                }
            });
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

}
