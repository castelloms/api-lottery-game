package com.lottery.game.util;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.paukov.combinatorics3.PermutationGenerator;

import com.lottery.game.exception.InvalidNumberOfGamesException;

class GeneratePermutationTest {

    @Test
    void shouldntHaveDuplicatePermutationToSize27() {
        List<List<Integer>> permutations = GeneratePermutation.generateRandomPermutations(27);
        assertFalse(PermutationGenerator.hasDuplicates(permutations));
    }

    @Test
    void shouldntGenerateRandomPermutationsWithSizeGreaterThan27() {
        assertThrows(InvalidNumberOfGamesException.class, () -> GeneratePermutation.generateRandomPermutations(30));
    }

}
