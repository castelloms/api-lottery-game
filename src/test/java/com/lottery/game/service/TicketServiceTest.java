package com.lottery.game.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import com.lottery.game.entity.Game;
import com.lottery.game.entity.Ticket;
import com.lottery.game.enumeration.Status;
import com.lottery.game.exception.InvalidNumberOfGamesException;
import com.lottery.game.exception.ResourceNotFoundException;
import com.lottery.game.exception.TicketCheckedException;
import com.lottery.game.repository.TicketRepository;
import com.lottery.game.request.GameRequest;
import com.lottery.game.request.TicketRequest;
import com.lottery.game.response.TicketResponse;

@WebMvcTest(TicketService.class)
class TicketServiceTest {

    @Autowired
    private TicketService ticketService;

    @MockBean
    private TicketRepository ticketRepository;

    private String ticketId;

    private TicketRequest ticketRequest;

    private GameRequest gameRequest;

    private TicketResponse ticketResponse;

    private Page<Ticket> tickets;

    private Game game;

    @Test
    void shouldCreateTicketSuccessfully() {
        givenAValidTicketRequest();
        givenTicketRepositorySaveSuccessfully();
        whenCallCreateTicket();
        thenExpectedTicketNotEmpty();
    }

    @Test
    void shouldntCreateTicketWithGamesQuantityNegative() {
        givenATicketRequestWithGamesQuantityNegative();
        thenThrowInvalidNumberOfGamesExceptionWhenCallCreateTicket();
    }

    @Test
    void shouldntCreateTicketWithGamesQuantityZero() {
        givenATicketRequestWithGamesQuantityZero();
        thenThrowInvalidNumberOfGamesExceptionWhenCallCreateTicket();
    }

    @Test
    void shouldFindTicketByIdSuccessfully() {
        givenAValidTicketId();
        givenTicketRepositoryFindTicketByIdReturnTicketWithOneGame();
        whenCallFindTicketById();
        thenExpectedTicketResponse();
    }

    @Test
    void shouldntFindTicketByIdThrowResourceNotFoundException() {
        givenAValidTicketId();
        givenTicketRepositoryFindTicketByIdReturnEmpty();
        thenThrowResourceNotFoundExceptionWhenCallFindTicketById();
    }

    @Test
    void shouldFindTicketsSuccessfully() {
        givenAValidTicketId();
        givenTicketRepositoryFindTicketsReturnOneTicketWithOneGame();
        whenCallFindTickets();
        thenExpectOneTicketResponse();
    }

    @Test
    void shouldAddGameToTicketSuccessfuly() {
        givenAValidTicketId();
        givenAValidGameRequest();
        givenTicketRepositoryFindTicketByIdReturnTicketWithOneGame();
        givenTicketRepositorySaveSuccessfully();
        whenCallUpdateTicket();
        thenExpectTicketRepositorySaveCall();
    }

    @Test
    void shouldntAddGameToTicketWithStatusChecked() {
        givenAValidTicketId();
        givenAValidTicketRequest();
        givenTicketRepositoryFindTicketByReturnStatusChecked();
        thenThrowTicketCheckedExceptionWhenCallUpdateTicket();
    }

    @Test
    void shouldUpdateTicketStatusSuccessfully() {
        givenAValidTicketId();
        givenAValidTicketRequest();
        givenTicketRepositoryFindTicketByIdReturnTicketWithOneGame();
        whenCallUpdateTicketStatus();
        thenExpectTicketRepositorySaveCall();
        thenExpectedTicketWithStatuChecked();
    }

    @Test
    void shouldntUpdateTicketStatusThrowResourceNotFountException() {
        givenAValidTicketId();
        givenAValidGameRequest();
        givenTicketRepositoryFindTicketByIdReturnEmpty();
        thenThrowResourceNotFoundExceptionWhenCallUpdateTicket();
    }

    // given methods
    private void givenAValidTicketId() {
        ticketId = UUID.randomUUID().toString();
    }

    private void givenAValidTicketRequest() {
        ticketRequest = new TicketRequest(7);
    }

    private void givenAValidGameRequest() {
        gameRequest = new GameRequest(0, 1, 1);
    }

    private void givenATicketRequestWithGamesQuantityNegative() {
        ticketRequest = new TicketRequest(-1);
    }

    private void givenATicketRequestWithGamesQuantityZero() {
        ticketRequest = new TicketRequest(0);
    }

    private void givenTicketRepositoryFindTicketByReturnStatusChecked() {
        doReturn(Optional.of(Ticket.builder().status(Status.CHECKED).build())).when(ticketRepository)
                .findById(any(String.class));
    }

    private void givenTicketRepositoryFindTicketByIdReturnTicketWithOneGame() {
        List<Game> games = new ArrayList<>();
        Ticket ticket = Ticket.builder()
                .id(ticketId)
                .games(games)
                .build();
        buildGame(ticket);
        games.add(game);
        doReturn(Optional.of(ticket)).when(ticketRepository).findById(any(String.class));
    }

    private void givenTicketRepositoryFindTicketsReturnOneTicketWithOneGame() {
        List<Game> games = new ArrayList<>();
        Ticket ticket = Ticket.builder()
                .id(ticketId)
                .games(games)
                .build();
        buildGame(ticket);
        games.add(game);
        games.add(game);
        doReturn(new PageImpl<>(List.of(ticket))).when(ticketRepository).findAll(any(PageRequest.class));
    }

    private void givenTicketRepositorySaveSuccessfully() {
        Ticket ticket = Ticket.builder()
                .createdAt(LocalDateTime.now())
                .build();
        buildGame(ticket);
        ticket.setGames(List.of(game));
        doReturn(ticket).when(ticketRepository).save(any(Ticket.class));
    }

    private void givenTicketRepositoryFindTicketByIdReturnEmpty() {
        doReturn(Optional.empty()).when(ticketRepository).findById(any(String.class));
    }

    // when methods
    private void whenCallCreateTicket() {
        ticketResponse = ticketService.createTicket(ticketRequest);
    }

    private void whenCallFindTicketById() {
        ticketResponse = ticketService.findTicketById(ticketId);
    }

    private void whenCallFindTickets() {
        tickets = ticketService.findTickets(PageRequest.of(0, 3));
    }

    private void whenCallUpdateTicket() {
        ticketService.addGameToTicket(ticketId, gameRequest);
    }

    private void whenCallUpdateTicketStatus() {
        ticketResponse = ticketService.updateTicketStatus(ticketId);
    }

    // then methods
    private void thenExpectedTicketNotEmpty() {
        assertNotNull(ticketResponse);
    }

    private void thenThrowInvalidNumberOfGamesExceptionWhenCallCreateTicket() {
        assertThrows(InvalidNumberOfGamesException.class, () -> ticketService.createTicket(ticketRequest));
    }

    private void thenThrowResourceNotFoundExceptionWhenCallFindTicketById() {
        assertThrows(ResourceNotFoundException.class, () -> ticketService.findTicketById(ticketId));
    }

    private void thenThrowTicketCheckedExceptionWhenCallUpdateTicket() {
        assertThrows(TicketCheckedException.class, () -> ticketService.addGameToTicket(ticketId, gameRequest));
    }

    private void thenThrowResourceNotFoundExceptionWhenCallUpdateTicket() {
        assertThrows(ResourceNotFoundException.class, () -> ticketService.addGameToTicket(ticketId, gameRequest));
    }

    private void thenExpectedTicketResponse() {
        assertEquals(ticketId, ticketResponse.getTicketId());
        assertEquals(Status.PENDING, ticketResponse.getStatus());
        assertEquals(Status.PENDING, ticketResponse.getStatus());
        assertEquals(1, ticketResponse.getGamesCount());
        assertEquals(10, ticketResponse.getGames().get(0).getResult());
    }

    private void thenExpectOneTicketResponse() {
        assertEquals(1, tickets.getContent().size());
    }

    private void thenExpectTicketRepositorySaveCall() {
        verify(ticketRepository, times(1)).save(any(Ticket.class));
    }

    private void thenExpectedTicketWithStatuChecked() {
        assertEquals(ticketId, ticketResponse.getTicketId());
    }

    // Other methods
    private void buildGame(Ticket ticket) {
        game = new Game(ticket, 0, 2, 0);
        game.calculateResult();
    }

}
