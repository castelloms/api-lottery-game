package com.lottery.game.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lottery.game.entity.Ticket;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, String> {

}
