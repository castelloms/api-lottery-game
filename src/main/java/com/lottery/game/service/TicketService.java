package com.lottery.game.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.lottery.game.entity.Game;
import com.lottery.game.entity.Ticket;
import com.lottery.game.enumeration.Status;
import com.lottery.game.exception.InvalidNumberOfGamesException;
import com.lottery.game.exception.ResourceNotFoundException;
import com.lottery.game.exception.TicketCheckedException;
import com.lottery.game.repository.TicketRepository;
import com.lottery.game.request.GameRequest;
import com.lottery.game.request.TicketRequest;
import com.lottery.game.response.TicketResponse;
import com.lottery.game.util.GeneratePermutation;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class TicketService {

    private final TicketRepository ticketRepository;

    @Transactional
    public TicketResponse createTicket(TicketRequest ticketRequest) {
        validateNumberOfGames(ticketRequest.getNumberOfGames());
        Ticket ticket = new Ticket();
        List<Game> games = GeneratePermutation.generateRandomPermutations(ticketRequest.getNumberOfGames())
                .stream()
                .map(permutation -> {
                    Game game = new Game(ticket, permutation.get(0), permutation.get(1), permutation.get(2));
                    game.calculateResult();
                    return game;
                }).collect(Collectors.toList());
        ticket.setGames(games);
        return ticketRepository.save(ticket).toResponse();
    }

    public TicketResponse findTicketById(String ticketId) {
        return ticketRepository.findById(ticketId)
                .map(Ticket::toResponse)
                .orElseThrow(ResourceNotFoundException::new);
    }

    public Page<Ticket> findTickets(Pageable pageable) {
        return ticketRepository.findAll(pageable);
    }

    public void addGameToTicket(String ticketId, GameRequest gameRequest) {
        Ticket ticket = ticketRepository.findById(ticketId).orElseThrow(ResourceNotFoundException::new);
        if (ticket.isChecked()) {
            throw new TicketCheckedException();
        }
        Game game = new Game(ticket, gameRequest.getFirstNumber(), gameRequest.getSecondNumber(),
                gameRequest.getThirdNumber());
        game.calculateResult();
        ticket.getGames().add(game);
        ticketRepository.save(ticket);
    }

    @Transactional
    public TicketResponse updateTicketStatus(String ticketId) {
        Ticket ticket = ticketRepository.findById(ticketId).orElseThrow(ResourceNotFoundException::new);
        if (!ticket.isChecked()) {
            ticket.setStatus(Status.CHECKED);
            ticketRepository.save(ticket);
        }
        return ticket.toResponse();
    }

    private void validateNumberOfGames(int gamesCount) {
        if (gamesCount <= 0 || gamesCount > 27) {
            throw new InvalidNumberOfGamesException();
        }
    }

}
