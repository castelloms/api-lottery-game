package com.lottery.game.exception;

public class TicketCheckedException extends RuntimeException {

    private static final long serialVersionUID = 5716408692515873405L;

    public TicketCheckedException() {
        super(ErrorMessage.TICKET_CHECKED);
    }

}
