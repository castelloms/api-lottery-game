package com.lottery.game.exception;

public class InvalidNumberException extends RuntimeException {

    private static final long serialVersionUID = -4084578272338068657L;

    public InvalidNumberException() {
        super(ErrorMessage.INVALID_NUMBER);
    }

}
