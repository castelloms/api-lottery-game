package com.lottery.game.exception;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class ErrorMessage {

    public static final String INVALID_NUMBER_OF_GAMES = "Games number must be between 1 and 27";
    public static final String TICKET_CHECKED = "Ticket has been checked and it's not allowed adding a new game.";
    public static final String INVALID_NUMBER = "Numbers must be between 0 and 2";

}
