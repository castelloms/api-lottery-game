package com.lottery.game.exception;

public class InvalidNumberOfGamesException extends RuntimeException {

    private static final long serialVersionUID = 298224529836179695L;

    public InvalidNumberOfGamesException() {
        super(ErrorMessage.INVALID_NUMBER_OF_GAMES);
    }

}
