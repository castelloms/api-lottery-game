package com.lottery.game.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.lottery.game.exception.InvalidNumberException;
import com.lottery.game.exception.InvalidNumberOfGamesException;
import com.lottery.game.exception.Message;
import com.lottery.game.exception.ResourceNotFoundException;
import com.lottery.game.exception.TicketCheckedException;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Message> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex) {
        Message message = new Message(ex.getMessage());
        return ResponseEntity.badRequest().body(message);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Void> handleNotFoundException(ResourceNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(InvalidNumberOfGamesException.class)
    public ResponseEntity<Message> handleInvalidNumberOfGamesException(InvalidNumberOfGamesException ex) {
        Message message = new Message(ex.getMessage());
        return ResponseEntity.badRequest().body(message);
    }

    @ExceptionHandler(TicketCheckedException.class)
    public ResponseEntity<Message> handleTicketProcessedException(TicketCheckedException ex) {
        Message message = new Message(ex.getMessage());
        return ResponseEntity.unprocessableEntity().body(message);
    }

    @ExceptionHandler(InvalidNumberException.class)
    public ResponseEntity<Message> handleInvalidNumberException(InvalidNumberException ex) {
        Message message = new Message(ex.getMessage());
        return ResponseEntity.badRequest().body(message);
    }

}
