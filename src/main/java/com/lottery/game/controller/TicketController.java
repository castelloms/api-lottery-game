package com.lottery.game.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.lottery.game.entity.Ticket;
import com.lottery.game.request.GameRequest;
import com.lottery.game.request.TicketRequest;
import com.lottery.game.response.TicketResponse;
import com.lottery.game.service.TicketService;
import com.lottery.game.validator.GameRequestValidator;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/tickets")
public class TicketController {

    private final TicketService ticketService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TicketResponse> createTicket(@RequestBody TicketRequest ticketRequest) {
        TicketResponse ticketResponse = ticketService.createTicket(ticketRequest);
        return ResponseEntity.created(ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{ticketId}")
                .buildAndExpand(ticketResponse.getTicketId())
                .toUri())
                .body(ticketResponse);
    }

    @GetMapping(path = "/{ticketId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TicketResponse> getTicket(@PathVariable String ticketId) {
        TicketResponse ticketResponse = ticketService.findTicketById(ticketId);
        return ResponseEntity.ok(ticketResponse);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TicketResponse>> getTickets(@RequestParam(defaultValue = "0", required = false) int page,
            @RequestParam(defaultValue = "10", required = false) int size) {
        Page<Ticket> tickets = ticketService.findTickets(PageRequest.of(page, size, Sort.by("createdAt").descending()));
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_RANGE, String.valueOf(tickets.getTotalElements()))
                .body(tickets.getContent().stream().map(Ticket::toResponse).collect(Collectors.toList()));
    }

    @PostMapping(path = "/{ticketId}/games", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> addGame(@PathVariable String ticketId, @RequestBody GameRequest gameRequest) {
        GameRequestValidator.validate(gameRequest);
        ticketService.addGameToTicket(ticketId, gameRequest);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping(path = "/{ticketId}/status")
    public ResponseEntity<TicketResponse> updateTicketStatus(@PathVariable String ticketId) {
        TicketResponse ticketResponse = ticketService.updateTicketStatus(ticketId);
        return ResponseEntity.ok(ticketResponse);
    }

}
