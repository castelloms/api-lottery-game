package com.lottery.game.entity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import com.lottery.game.enumeration.Status;
import com.lottery.game.response.TicketResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "TICKET")
public class Ticket {

    @Id
    @Column(name = "TICKET_ID")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @CreationTimestamp
    @Column(name = "CREATED_AT")
    private LocalDateTime createdAt;

    @Default
    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private Status status = Status.PENDING;

    @Default
    @OneToMany(mappedBy = "ticket", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Game> games = List.of();

    public TicketResponse toResponse() {
        return TicketResponse.builder()
                .ticketId(id)
                .creationDate(createdAt)
                .status(status)
                .gamesCount(games.size())
                .games(games.stream()
                        .sorted(Game::sortByResult)
                        .map(Game::toResponse)
                        .collect(Collectors.toList()))
                .build();
    }

    public boolean isChecked() {
        return status == Status.CHECKED;
    }

}
