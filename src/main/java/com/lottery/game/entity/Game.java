package com.lottery.game.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.lottery.game.response.GameResponse;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
@Entity(name = "GAME")
public class Game {

    private static final int LESS_THAN_ZERO = 0;
    private static final int GREATER_THAN_TWO = 2;

    @Id
    @Column(name = "GAME_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "TICKET_ID", nullable = false)
    private Ticket ticket;

    @Column(name = "FIRST_NUMBER", nullable = false)
    private Integer firstNumber;

    @Column(name = "SECOND_NUMBER", nullable = false)
    private Integer secondNumber;

    @Column(name = "THIRD_NUMBER", nullable = false)
    private Integer thirdNumber;

    @Column(name = "RESULT", nullable = false)
    private Integer result = 0;

    public Game(Ticket ticket, Integer firstNumber, Integer secondNumber, Integer thirdNumber) {
        this.ticket = ticket;
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.thirdNumber = thirdNumber;
    }

    /**
     * The logic to calculate its result.
     *
     */
    public void calculateResult() {
        if (invalidNumber(firstNumber) || invalidNumber(secondNumber) || invalidNumber(thirdNumber)) {
            result = 0;
        } else if (firstNumber + secondNumber + thirdNumber == 2) {
            result = 10;
        } else if (firstNumber.equals(secondNumber) && firstNumber.equals(thirdNumber)) {
            result = 5;
        } else if (!firstNumber.equals(secondNumber) && !firstNumber.equals(thirdNumber)) {
            result = 1;
        }
    }

    /**
     * Sort result in descending order
     */
    public int sortByResult(Game otherGame) {
        return Integer.compare(otherGame.getResult(), result);
    }

    public GameResponse toResponse() {
        return new GameResponse(List.of(firstNumber, secondNumber, thirdNumber), result);
    }

    private boolean invalidNumber(Integer number) {
        return number == null || number < LESS_THAN_ZERO || number > GREATER_THAN_TWO;
    }

}
