package com.lottery.game.request;

import com.lottery.game.exception.InvalidNumberException;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class GameRequest {

    private Integer firstNumber;
    private Integer secondNumber;
    private Integer thirdNumber;

    public boolean hasInvalidNumber() {
        return numberGreaterorEqualsToZeroAndLessOrEqualToTwo(firstNumber)
                || numberGreaterorEqualsToZeroAndLessOrEqualToTwo(secondNumber)
                || numberGreaterorEqualsToZeroAndLessOrEqualToTwo(thirdNumber);
    }

    private boolean numberGreaterorEqualsToZeroAndLessOrEqualToTwo(Integer number) {
        if (number == null) {
            throw new InvalidNumberException();
        }
        return number < 0 || number > 2;
    }

}
