package com.lottery.game.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class GameResponse {

    private List<Integer> numbers;
    private Integer result;

}
