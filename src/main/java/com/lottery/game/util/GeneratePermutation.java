package com.lottery.game.util;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.paukov.combinatorics3.Generator;

import com.lottery.game.exception.InvalidNumberOfGamesException;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GeneratePermutation {

    public static final int MAX_SIZE_PERMUTATION = 27;

    private static List<List<Integer>> permutaions = Generator.permutation(0, 1, 2)
            .withRepetitions(3)
            .stream()
            .collect(Collectors.toList());

    public static List<List<Integer>> generateRandomPermutations(int size) {
        if (size > MAX_SIZE_PERMUTATION) {
            throw new InvalidNumberOfGamesException();
        }
        Collections.shuffle(permutaions);
        return permutaions.subList(0, size);
    }

}
