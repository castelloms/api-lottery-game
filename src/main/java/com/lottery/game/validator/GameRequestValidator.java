package com.lottery.game.validator;

import com.lottery.game.exception.InvalidNumberException;
import com.lottery.game.request.GameRequest;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GameRequestValidator {

    public static void validate(GameRequest gameRequest) {
        if (gameRequest.hasInvalidNumber()) {
            throw new InvalidNumberException();
        }
    }

}
