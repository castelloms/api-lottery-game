# API Lottery Game

Simple lottery system.

**  Lottery Rules ** 

You have a series of lines on a ticket with 3 numbers, each of which has a value of 0, 1, or 2. For each ticket if the sum of the values on a line is 2, the result for that line is 10. Otherwise if they are all the same, the result is 5. Otherwise so long as both 2nd and 3rd numbers are different from the 1st, the result is 1. Otherwise the result is 0.

** Implementation ** 

Implement a REST interface to generate a ticket with n lines. Additionally we will need to have the ability to check the status of lines on a ticket. We would like the lines sorted into outcomes before being returned. It should be possible for a ticket to be amended with n additional lines. Once the status of a ticket has been checked it should not be possible to amend.

## Prerequisites

You have installed:

 - Java 11
 - Docker 19.03.8
 - Docker Compose 1.22.0

## Generate and initialize application

** How to compile and test **

```sh
./mvnw clean package
```

** How to generate image **

```sh
sh docker rm -f api-lottery && docker build -t castello/api-lottery .
```

** How to start images **

```sh
docker-compose -f docker/docker-compose.yml up -d mysql-lottery api-lottery
```

# API Definition

Base URL: http://localhost:8080

** Tickets **

- POST  /tickets
- GET   /tickets
- GET   /tickets/{ticketId}
- POST  /tickets/{ticketId}/games
- PATCH /tickets/{ticketId}/status

** Actuator **

- GET /actuator/health
- GET /actuator/metrics

** Swagger documentation **

The file in the folder swagger can be imported [here](https://editor.swagger.io/) to see the API definition.

```sh
swagger/api-lottery.yaml
```

** Postman collection **

The file in the folder postman can be imported in the postman app.

```sh
postman/LotteryGame.postman_collection.json
```
