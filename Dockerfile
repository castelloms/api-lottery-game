FROM openjdk:11-jdk-slim
LABEL maintainer Marcelo Castello <castelloms@gmail.com>

WORKDIR /castello
ADD target/api-lottery.jar app.jar

CMD java $JAVA_OPTS -jar app.jar
